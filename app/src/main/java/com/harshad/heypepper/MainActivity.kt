package com.harshad.heypepper


import android.os.Bundle
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.`object`.conversation.Chat
import com.aldebaran.qi.sdk.`object`.conversation.QiChatbot
import com.aldebaran.qi.sdk.`object`.conversation.Topic
import com.aldebaran.qi.sdk.`object`.locale.Language
import com.aldebaran.qi.sdk.`object`.locale.Locale
import com.aldebaran.qi.sdk.`object`.locale.Region
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : RobotActivity(), RobotLifecycleCallbacks {

    var mQiContext: QiContext? = null
    lateinit var chatbot: QiChatbot
    lateinit var chat: Chat
    private lateinit var topic: Topic
    private var chatFuture: Future<Void>? = null
    private var locale = Locale(Language.ENGLISH, Region.UNITED_STATES)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //initialize QiSDK
        QiSDK.register(this, this)
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {
        mQiContext = qiContext
        onConversationStarted()
    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun onConversationStarted() {
        GlobalScope.launch {
            try {
                topic = TopicBuilder.with(mQiContext).withAsset("intro.top").build()
                initChatBoat()
                initChat()
                chatFuture = chat.async().run()
                chat.addOnStartedListener {
                    onConversationStarted()
                }
            } catch (exc: Exception) {

            }
        }
    }

    private fun initChat() {
        chat = ChatBuilder.with(mQiContext)
            .withChatbot(chatbot).withLocale(locale)
            .build()
    }

    private fun initChatBoat() {
        if (::chatbot.isInitialized) {
            chatbot.removeAllOnBookmarkReachedListeners()
        }
        chatbot = QiChatbotBuilder.with(mQiContext)
            .withTopic(topic).withLocale(locale)
            .build()
    }

    override fun onRobotFocusLost() {
        //TODO("Not yet implemented")
    }

    override fun onRobotFocusRefused(reason: String?) {
        //TODO("Not yet implemented")
    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }
}